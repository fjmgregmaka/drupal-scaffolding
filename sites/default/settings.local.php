<?php
  include_once( __DIR__ . '/settings.local.db.php' );
  
  $base_url = 'http://drupal.docker.localhost:8000';
  
  $settings['hash_salt'] = '1fdee27c2a455ced03697b927c6e7293bce33800d2cc716d64ad8670839a96e2';
  //$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
  /**
    * Show all error messages, with backtrace information.
    *
    * In case the error level could not be fetched from the database, as for
    * example the database connection failed, we rely only on this value.
  */
  $config['system.logging']['error_level'] = 'verbose';

  /**
  * Disable CSS and JS aggregation.
  */
  $config['system.performance']['css']['preprocess'] = FALSE;
  $config['system.performance']['js']['preprocess'] = FALSE;

  ini_set('memory_limit', '256M');

  //$settings['cache']['bins']['render'] = 'cache.backend.null';
  //$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
  #$settings['cache']['bins']['page'] = 'cache.backend.null';
  /*$settings['trusted_host_patterns'] = array(
    '.*\.192\.168',
    '^localhost',
    '^*\.localhost',
  );*/

$config_directories[CONFIG_SYNC_DIRECTORY] = '../config/sync';

$settings['install_profile'] = 'standard';
